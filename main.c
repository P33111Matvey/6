#include "bmp.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>


int main(){
	char* filename;
	char* outputf;
	struct bmp_header *header;
	FILE *inputfile;
	FILE *outputfile;
	struct pixel *pixeldata;
	struct pixel *pixelbuffer;
	int datasize = 0;
	uint32_t swaptmp = 0;
	int x =0, y = 0;
	filename = "./input.bmp";
	outputf = "./out.bmp";
	int saveerro;
	header = (struct bmp_header*)malloc(sizeof(struct bmp_header));
	errno = 0;
    inputfile = fopen(filename, "rb");
    if (errno || ferror(inputfile)) {BadReadExit(0);}	
    errno = 0;
    fread(header, sizeof(struct bmp_header), 1, inputfile);
	if (errno || ferror(inputfile)){BadReadExit(1);}
	errno = 0;

	datasize = header->biWidth * header->biHeight;
	pixeldata = (struct pixel*)malloc(datasize * sizeof(struct pixel));
	printf("0x%08x\n", pixeldata);
	for(x = 0; x < header->biHeight; x++){
		if(!readPixelRow(pixeldata + (x*  header->biWidth),  header->biWidth, inputfile)){
			BadReadExit(2);
		}
	}
	errno = 0;
	fclose(inputfile);
	if (errno || ferror(inputfile)){ BadReadExit(4);}
	errno = 0;
	printf("Height: %d px\n", header->biHeight);
	printf("Width: %d px\n", header->biWidth);
	/*for(x = 0; x < datasize; x++){
		printf("pixel %d: %d r %d g %d b\n", x, pixeldata[x].r, pixeldata[x].g,pixeldata[x].b);
	}*/
	
	swaptmp = header->biWidth;
	header->biWidth = header->biHeight;
	header->biHeight = swaptmp;
	outputfile = fopen(outputf, "wb");
	if (errno || ferror(outputfile)){ BadWriteExit(0);}
	errno = 0;
	fwrite(header, sizeof(struct bmp_header), 1, outputfile);
	if (errno || ferror(outputfile)) {BadWriteExit(1);}
	
	pixelbuffer = (struct pixel*)malloc(header->biWidth * sizeof(struct pixel));
	errno = 0;
	//rotation 90 clockwise
	for(y = header->biHeight - 1;y >=0;y--){
		for(x = 0;x<header->biWidth;x++){
			pixelbuffer[x] = *(pixeldata + (x*header->biHeight) + y);
		}
		if(!writePixelRow(pixelbuffer, header->biWidth,outputfile)){
			fclose(outputfile);
			BadWriteExit(2);
		}
	}
	
	/*for(y = 0;y < header->biHeight;y++){
		for(x = 0;x < header->biWidth;x++){
			pixelbuffer[x] = *(pixeldata + ((header->biWidth-1-x)*header->biHeight) + y);
		}
		if(!writePixelRow(pixelbuffer, header->biWidth,outputfile)){
			fclose(outputfile);
			BadWriteExit(2);
		}
	}
    */
	errno = 0;
	fflush(outputfile);
	if (errno || ferror(outputfile)) {BadWriteExit(3);}
	fclose(outputfile);
	if (errno || ferror(outputfile)) {BadWriteExit(3);}
	errno = 0;
	printf("Done\n");
	return 0;
}
