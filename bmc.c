#include "bmp.h"
#include <stdio.h>
#include <errno.h>
int writePixelRow(struct pixel* pixels, int count, FILE* file){
	char zeros[4] = {0, 0, 0, 0};
	errno = 0;
	fwrite(pixels, sizeof(struct pixel), count, file);
	if (errno || ferror(file)) return 0;
    errno = 0;
	int extraread = ((count*3)%4 == 0)?(0):(4-((count*3)%4));
	fwrite(zeros, 1, extraread, file);
	if (errno || ferror(file)) return 0;
	errno = 0;	
	return 1;
}
int readPixelRow(struct pixel* pixels, int count, FILE* file)
{
	printf("0x%08x\n", pixels);
	char *zeros = (char*)malloc(sizeof(char)*4);
	errno = 0;
	fread(pixels, sizeof(struct pixel), count, file);
	if (errno || ferror(file)) return 0;
    errno = 0;
	int extraread = ((count*3)%4 == 0)?(0):(4-((count*3)%4));
	fread(zeros, 1, extraread, file);
	if (errno || ferror(file)) return 0;
	errno = 0;
	free(zeros);
	return 1;
}