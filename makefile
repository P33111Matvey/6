all: program

program: main.c bmp.c
	gcc -o main main.c bmp.c

run: program
	./main

clean:
	rm main
